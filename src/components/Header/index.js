import "./index.css";

const Header = (props) => {
  const { userInput,inputValueUpdater } = props;
  const updateInputData = (event) => {
    inputValueUpdater(event.target.value);
  };

  return (
    <div className="header-container">
      <h1 className="main-heading">People of GOT</h1>
      <input
        placeholder="Enter Name"
        className="user-input"
        type="text"
        onChange={updateInputData}
        value={userInput}
      />
    </div>
  );
};

export default Header;
