import { Component } from "react";
import Header from "../Header";
import FamilyName from "../FamilyName";
import Individule from "../People";
import { peoplesData, familyData } from "../../data";
import "./App.css";

class App extends Component {
  state = { userInput: "", data: peoplesData, activeFamily: "" };


// Updating peoples list by familyName

  updatePeopleByFamily = (family) => {
    const updated = peoplesData.filter((eachfamily) => eachfamily.name === family);
    this.setState({ data: updated, activeFamily: family });
  };

// Updating peoples list by searchInput

  updateUserInput = (input) => {
    const updatedData = peoplesData.reduce((acc, curr) => {
      const people = curr.people.filter((eachPerson) =>eachPerson.name.toLowerCase().includes(input.toLowerCase()));

      let tempDataUpdater = {};
      tempDataUpdater["name"] = curr.name;
      tempDataUpdater["people"] = people;
      acc = [...acc, tempDataUpdater];
      return acc;

    }, []);

    this.setState({
      userInput: input,
      data: updatedData,
      activeFamily:""
    });
  };


  render() {
    const {input, data, activeFamily } = this.state;

    return (
      <>
        <Header userInput={input} inputValueUpdater={this.updateUserInput} />

        {/* Search by Family name buttons */}

        <div className="buttons-container">
          {familyData.map((eachfamily) => (
            <FamilyName
              updatePeopleByFamily={this.updatePeopleByFamily}
              family={eachfamily}
              activeFamily={activeFamily}
              key={eachfamily}
            />
          ))}
        </div>

        {/* peoples list */}

        <ul>
          {data.map((eachPerson) =>
            eachPerson.people.map((each) => (
              <Individule eachIndividule={each} key={each.id} />
            ))
          )}
        </ul>
      </>
    );
  }
}

export default App;
