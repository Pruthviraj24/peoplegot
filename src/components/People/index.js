import "./index.css";

const Individule = (props) => {
  const { eachIndividule } = props;

  return (
    <li className="person-item">
      <img
        className="profilePic"
        src={eachIndividule.image}
        alt={eachIndividule.name}
      />
      <h5 className="person-name-header">{eachIndividule.name}</h5>
      <p>{eachIndividule.description}</p>
      <button type="button" className="know-more-button">
        <a href={eachIndividule.wikiLink}>Know More</a>
      </button>
    </li>
  );
};

export default Individule;
