import "./index.css";

const FamilyName = (props) => {
  const { updatePeopleByFamily, family,activeFamily} = props;

  //Passing family name to update peoples list
  const updateUsersList = (event) => {
    updatePeopleByFamily(event.target.value);
  };

  // changing active button className 
  const activeFamilyButton = activeFamily !== family ? "family-name-button" : "active-family-button"
  
  return (
    <>
      <button
        className={activeFamilyButton}
        type="button"
        onClick={updateUsersList}
        value={family}
      >
        {family}
      </button>
    </>
  );
};

export default FamilyName;
